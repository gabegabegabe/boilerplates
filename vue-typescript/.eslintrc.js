module.exports = {
	extends: [
		'@gabegabegabe/eslint-config',
		'@gabegabegabe/eslint-config/parcel'
	],
	overrides: [
		{
			files: ['*.ts', '*.vue'],
			extends: [
				'@gabegabegabe/eslint-config/typescript',
				'@gabegabegabe/eslint-config/vue',
				'@gabegabegabe/eslint-config/vue-typescript'
			]
		}
	]
};
