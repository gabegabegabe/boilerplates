module.exports = {
	extends: ['@gabegabegabe/eslint-config'],
	overrides: [
		{
			files: '*.js',
			extends: ['@gabegabegabe/eslint-config/javascript']
		}
	]
};
