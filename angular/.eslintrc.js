module.exports = {
	extends: ['@gabegabegabe/eslint-config'],
	overrides: [
		{
			files: ['*.js'],
			extends: ['@gabegabegabe/eslint-config/javascript']
		},
		{
			files: ['*.ts'],
			extends: [
				'@gabegabegabe/eslint-config/angular',
				'@gabegabegabe/eslint-config/typescript'
			]
		},
		{
			files: ['*.html'],
			extends: ['@gabegabegabe/eslint-config/angular-html']
		}
	]
};
