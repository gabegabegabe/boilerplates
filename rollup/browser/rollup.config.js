import alias from '@rollup/plugin-alias';
import babel from '@rollup/plugin-babel';
import { eslint } from 'rollup-plugin-eslint';
import fs from 'fs';
import path from 'path';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

const ASSET_FILE_TYPES = [];
const SCRIPT_FILE_TYPES = ['.js']
const FILE_TYPES = [...ASSET_FILE_TYPES, ...SCRIPT_FILE_TYPES];

export default {
	input: 'src/index.js',
	output: {
		dir: 'dist',
		format: 'umd',
		sourcemap: true
	},
	plugins: [
		resolve({ browser: true, extensions: FILE_TYPES }),
		alias({
			'~': path.join(__dirname, 'src'),
			resolve: FILE_TYPES
		}),
		eslint({ include: ['**/*.js'] }),
		babel({
			babelHelpers: 'bundled',
			extensions: SCRIPT_FILE_TYPES
			exclude: 'node_modules/**'
		}),
		terser()
	]
};
