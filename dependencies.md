# Dependencies
The following is a list of dev dependencies required for each project
type/module.

## Bundlers
### Parcel
- `parcel-bundler`
- `parcel-plugin-clean-dist`

#### PWA Support
- `parcel-plugin-pwa-manifest`
- `workbox-precaching`
- `workbox-routing`
- `workbox-strategies`

### Rollup
- `@rollup/plugin-alias`
- `@rollup/plugin-babel`
- `@rollup/plugin-commonjs` _(If targeting Node/CJS)_
- `@rollup/plugin-node-resolve`
- `builtin-modules`
- `rollup`
- `rollup-plugin-delete`
- `rollup-plugin-eslint`
- `rollup-plugin-terser`

## Environments
### Front-end
- `browserslist`

## Frameworks
### Vue
- `@vue/component-compiler-utils`
- `@vue/eslint-config-typescript` _(If using TypeScript)_
- `babel-eslint`
- `eslint-plugin-vue`
- `vue`
- `vue-eslint-parser`
- `vue-hot-reload-api`
- `vue-router` _(If using Vue routing)_
- `vue-template-compiler`
- `vuex` _(If using Vuex data store)_

## GraphQL
- `@gabegabegabe/eslint-config`
- `@graphql-eslint/eslint-plugin`
- `eslint`

## Markup
### HTML
- `htmlhint`

### Pug
- `@gabegabegabe/pug-lint-config`
- `pug`
- `pug-lint`

## Scripts
- `@babel/core`
- `@babel/preset-env`
- `@gabegabegabe/eslint-config`
- `core-js` _(If using unsupported ESx features)_
- `eslint`
- `regenerator-runtime` _(If using `async/await`)_

### JavaScript
- `jsdoc`

### TypeScript
- `@babel/plugin-proposal-class-properties`
- `@babel/preset-typescript`
- `@tsconfig/recommended`
- `@typescript-eslint/eslint-plugin`
- `@typescript-eslint/parser`
- `tslib`
- `typescript`

## Stylesheets
- `@gabegabegabe/stylelint-config`
- `autoprefixer` _(If not using Parcel v1)_
- `autoprefixer@9.8.6` _(If using Parcel v1)_
- `cssnano`
- `node-css-mqpacker`
- `postcss`
- `postcss-preset-env`
- `stylelint`

### SCSS
- `sass`
- `stylelint-scss`

## Unit Testing
### Ava
- `@ava/babel`
- `@babel/register`
- `ava`
- `babel-plugin-webpack-alias-7`

### Jest
- `@types/jest` _(If using TypeScript)_
- `jest`
