export default {
  mount: {
		static: { url: '/', static: true },
		src: { url: '/' }
  },
  plugins: [
		'@snowpack/plugin-vue'
  ],
	optimize: {
		bundle: true,
		minify: true,
		target: 'es2018'
	},
	devOptions: {
		port: 1234,
		open: 'none'
	},
	buildOptions: {
		sourcemap: true
	},
	alias: {
		'~': './src'
	}
};
