module.exports = {
	extends: [
		'@gabegabegabe/eslint-config',
		'@gabegabegabe/eslint-config/parcel'
	],
	overrides: [
		{
			files: ['*.js', '*.vue'],
			extends: [
				'@gabegabegabe/eslint-config/javascript',
				'@gabegabegabe/eslint-config/vue',
				'@gabegabegabe/eslint-config/nuxt'
			]
		}
	]
};
