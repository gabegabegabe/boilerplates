# package.json
Listed below are any modifications that may be necessary to `package.json` for
the listed project modules.

## Browsers Supported
```json
{
	"scripts": {
		"browsers": "browserslist"
	}
}
```

## Linting
**Note**: These assume the usage of JavaScript (front and back ends) and SCSS
(front end).  Substitute file extensions when to suit the project type.
### Front End
```json
{
	"scripts": {
		"lint:css": "stylelint src/**/*.scss",
		"lint:js": "eslint src/**/*.js"
	}
}
```

### Back End
```json
{
	"scripts": {
		"lint": "eslint src/**/*.js"
	}
}
```

### GraphQL
```json
{
	"scripts": {
		"lint:graphql": "eslint --ext=.graphql src/"
	}
}
```

## Parcel
```json
{
	"scripts": {
		"dev": "parcel [ENTRYPOINT]",
		"build": "parcel build [ENTRYPOINT]"
	}
}
```

## Parcel PWA
```json
{
	"pwa-manifest": {
		"name": "[NAME]",
		"shortName": "[SHORTNAME]",
		"startURL": "./",
		"display": "standalone",
		"scope": "/",
		"theme": "[COLOR]",
		"generateIconOptions": {
			"baseIcon": "[PATH RELATIVE TO PROJECT ROOT]",
			"sizes": [
				96,
				152,
				192,
				384,
				512
			],
			"genFavicons": true
		}
	}
}
```

## Pug
```json
{
	"scripts": {
		"lint:pug": "pug-lint src/**/*.pug"
	}
}
```

## Rollup
```json
{
	"scripts": {
		"dev": "rollup -cw",
		"build": "rollup -c"
	}
}
```

## Testing
### AVA
```json
{
	"scripts": {
		"test": "npm run build && ava"
	}
}
```

### Jest
```json
{
	"scripts": {
		"test": "jest"
	}
}
```

## TypeScript
```json
{
	"types": "dist/index.d.ts",
	"scripts": {
		"type-check": "tsc --noEmit",
		"type-check:watch": "npm run type-check -- --watch",
		"build:types": "tsc --emitDeclarationOnly",
		"build:js": "[BUNDLER_BUILD_COMMAND]",
		"build": "npm run build:types && npm run build:js"
	}
}
```
