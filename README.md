# Boilerplates
Contained here are dependencies and configuration files needed to get started
on various project types I frequently work on/with.

Contents of folders should be copied to project roots, in order of least
granular to most, e.g. `typescript` before `vue-typescript`.

`dependencies.md` contains dependencies needed for each of the project types.

`package.json.md` contains additional data for your `package.json` for any
project types that requires it.
