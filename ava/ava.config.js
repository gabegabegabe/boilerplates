export default {
	babel: {
		compileAsTests: ['test/_io-test.js'],
		testOptions: {
			plugins: [
				[
					'babel-plugin-webpack-alias-7',
					{
						config: './webpack.config.test.js'
					}
				]
			]
		}
	},
	files: ['test/**/*']
};
